const http = uni.$u.http

// 查询设备列表
export const getDevList = (params, options) => http.get('/device-info/list', {
  ...options,
  params
})

// 查询设备维保计划
export const getDeviceRepairMaintainPlanList = (params, options) => http.get('/device-repair-maintain-plan/list', {
  ...options,
  params
})

// 查询设备维保记录明细
export const getDeviceMaintainRepairRecordList = (params, options) => http.get('/device-maintain-repair-record/list', {
  ...options,
  params
})

// 更新设备维保记录明细
export const updateDeviceMaintainRepairRecord = (data, options = {}) => http.post('/device-maintain-repair-record/update', data, options)

// 上传文件
export const uploadFile = (filePath, options = {}) => http.upload('/file/upload', {
  ...options,
  filePath
})

// 获取隐患列表
export const getHiddenDangerInfoList = (params, options = {}) => http.get('/hidden-danger-info/list', {
  ...options,
  params
})

// 添加隐患
export const addHiddenDangerInfo = (data, options = {}) => http.post('/hidden-danger-info/add', data, options)

// 获取巡检计划列表
export const getInspectionPlanList = (params, options = {}) => http.get('/inspect-plan/list', {
  ...options,
  params
})

<<<<<<< HEAD
// 添加每日到站量
export const addStationAmount = (data, options = {}) => http.post('/gas-station-amount/add', data, options)

=======
// 获取巡检详情列表
export const getInspectDetailList = (params, options = {}) => http.get('/inspect-detail/list', {
  ...options,
  params
})

// 更新巡检点信息
export const updateInspectDetail = (data, options = {}) => http.post('/inspect-detail/update', data, options)

// 巡检轨迹上报
export const addInspectGeoInfo = (data, options = {}) => http.post('/geo/add-info', data, {
  ...options,
  baseURL: 'https://gtrq.gigsmart.cn:9999'
})
>>>>>>> 3470bca576dadc324ecb82d397d0fb5d6fa4b6a2
