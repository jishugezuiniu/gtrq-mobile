import { removeToken } from '@/utils/token.js'

const state = {
  
}

const mutations = {
  
}

const actions = {
  logout () {
    return new Promise((resolve, reject) => {
      removeToken()
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}