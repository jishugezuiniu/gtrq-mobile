const state = {
  isWorking: false,
  location: null,
}

const mutations = {
  SET_WORKING_STATUS (state, data) {
    state.isWorking = data
  },
  SET_LOCATION (state, data) {
    state.location = data
  }
}

const actions = {
  
}

const getters = {
  isWorking: state => state.isWorking,
  currentLocation: state => state.location
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}