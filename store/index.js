import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import user from './modules/user'
import inspection from './modules/inspection'

const store = new Vuex.Store({
  modules: {
    user,
    inspection
  }
})
export default store