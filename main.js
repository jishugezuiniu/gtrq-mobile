import App from './App'
import Vue from 'vue'
import store from './store'
import uView from '@/uni_modules/uview-ui'
import './uni.promisify.adaptor'

import '@/static/iconfont/iconfont.css';

Vue.use(uView)
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
  store,
  ...App
})

// 引入请求封装，将app参数传递到配置中
require('@/config/request.js')(app)

app.$mount()