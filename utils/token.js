/**
 * Token管理模块
 */
const TokenKey = 'yiqian_orchard'

export const getToken = () => uni.getStorageSync(TokenKey)

export const setToken = (token) => uni.setStorageSync(TokenKey, token)

export const removeToken = () => uni.removeStorageSync(TokenKey)
